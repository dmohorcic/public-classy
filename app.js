const http = require("http");
const fs = require("fs");
const rp = require('request-promise');
const $ = require('cheerio');
const path = require("path");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const port = process.env.PORT || 1337;
var json = [];
var locations;

function handleRequest(path, req, res) {
    fs.readFile(path, function(err, data) {
        if(err) {
            res.writeHead(404, {"Content-Type":"text/plain"});
            res.write("Stran "+req.url+" ne obstaja");
        } else {
            res.writeHead(200, {"Content-Type":"text/html"});
            res.write(data);
        }
        res.end();
    });
}

const server = http.createServer(function(req, res) {
    if(req.url === "/login") {
        if(req.method === "GET") {
            handleRequest("public/html/login.html", req, res);
        }
    } else if(req.url === "/") {
        handleRequest("public/html/index.html", req, res);
    } else if (req.url === "/register") {
        handleRequest("public/html/register.html", req, res);
    } else if (req.url.match("\.css$")) {
        var cssPath = path.join(__dirname, "public", req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type":"text/css"});
        fileStream.pipe(res);
    } else if(req.url.match("\.js$")) {
        var jsPath = path.join(__dirname, "public", req.url);
        var fileStream = fs.createReadStream(jsPath, "UTF-8");
        res.writeHead(200, {"Content-Type":"text/javascript"});
        fileStream.pipe(res);
    } else if(req.url.match("\.json$")) {
        var jsonPath = path.join(__dirname, "public", req.url);
        var fileStream = fs.createReadStream(jsonPath, "UTF-8");
        res.writeHead(200, {"Content-Type":"text/javascript"});
        fileStream.pipe(res);
    } else {
        res.writeHead(404, {"Content-Type":"text/plain"});
        res.end("Stran "+req.url+" ne obstaja");
    }
    console.log(req.url);
});

function myFunc() {
    const url = 'http://www.lpt.si/zapuscena_vozila/parkirisca';

    rp(url).then(function(html) {
        var len = $('tr', html).length;
        var obj = $('tr', html);
        //console.log($('tr', html)); //object
        //console.log(obj[3].children[9].children[0].children[0].data); //max capacity
        //console.log(obj[3].children[11].children[0].children[0].data); //avaliable places

        var json_arr = {};
        var counter = 0;

        for (var i = 0; i < len; i++) {
            if (obj[i].parent.name == 'tbody') {
                var name = obj[i].children[3].children[0].children[0].children[0].data;
                var max = obj[i].children[9].children[0].children[0].data;
                var free = obj[i].children[11].children[0].children[0].data;
                if (free == "/") {
                    free = -1;
                }
                if(name !== "Območja čas. om. park.") {
                    var x = 14.4643304994+(locations[name].x-458908)/77619.7881298;
                    var y = 46.0368497614+(locations[name].y-99325)/109625.9334262;
                    var str = '{"name":"'+name+'", "max":'+max+', "free":'+free+', "x":'+x+', "y":'+y+'}';
                } else {
                    var str = '{"name":"'+name+'", "max":'+max+', "free":'+free+'}';
                }
                json_arr[counter] = str;
                json.push(JSON.parse(str));
                counter++;
            }
        }
        console.log("DONE!");
        fs.writeFile("public/all_parkings.json", JSON.stringify(json), function(err) {
            if(err) {
                console.log(err);
            }
        });
    }).catch(function(err) {
        //handle error
    });
}

locations = JSON.parse(fs.readFileSync("lokacije.json"));

myFunc();
server.listen(port);
setInterval(myFunc, 60000); //every 1min refresh

console.log("Server running at http://localhost:%d", port);
