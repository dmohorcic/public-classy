/* global L, distance */
var pot;

var mapa;
var obmocje;

var json_all_parkhouses;
var markers = [];

const FRI_LAT = 46.050585;
const FRI_LNG = 14.499;

function redirect(lat, lng) {
    window.location = "http://maps.google.com/?ll="+lat+","+lng;
}

function drawParkhouses() {
    for(var i = 0; i < markers.length; i++) {
        mapa.removeLayer(markers[i]);
    }
    for(var i = 0; i < json_all_parkhouses.length; i++) {
        var obj = json_all_parkhouses[i];
        if(obj.free == -1) {obj.free = " - ";}

        var ikona = new L.Icon({
            iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-'+(obj.free == " - " ? 'blue':(obj.free < 10 ? 'red':'green'))+'.png',
            //shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });

        try {
            var marker = L.marker([obj.y, obj.x], {icon:ikona});
            var popup = "<div>"+obj.name+"<br>Prosta mesta: "+obj.free+" od "+obj.max+"<br>";
            popup += "<button onclick='redirect("+obj.y+","+obj.x+")'>Pojdi</button></div>";
            marker.bindPopup(popup).openPopup();
            marker.addTo(mapa);
            markers.push(marker);
        } catch(e) {console.log(e);}
    }
}

function getParkhouses() {
    var xobj = new XMLHttpRequest();
    xobj.open("GET", "all_parkings.json", true);
    xobj.onreadystatechange = function() {
        if(xobj.readyState == 4 && xobj.status == "200") {
            json_all_parkhouses = JSON.parse(xobj.responseText);
            setTimeout(drawParkhouses, 1000);
        }
    }
    xobj.send(null);
}

window.addEventListener('load', function () {
    getParkhouses();
    setInterval(getParkhouses, 60000);

    // Osnovne lastnosti mape
    var mapOptions = {
        center: [46.050585, 14.506811],
        zoom: 12,
        zoomControl: false
    };
    var zoomOptions = {
        position: "topright"
    }
    // Ustvarimo objekt mapa
    mapa = new L.map("mapa_id", mapOptions);

    document.getElementById("mapa_id").style.height = window.innerHeight-38+"px";
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    L.control.zoom(zoomOptions).addTo(mapa);
    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);




    // Ro�no dodamo fakulteto za ra�unalni�tvo in informatiko na mapo
    //dodajMarker(FRI_LAT, FRI_LNG, "FAKULTETA ZA RA�UNALNI�TVO IN INFORMATIKO", "FRI");

    // Objekt obla�ka markerja
    /*---------------------------------------------
    var popup = L.popup();

    
    function obKlikuNaMapo(e) {
        var latlng = e.latlng;
        //popup
          //.setLatLng(latlng)
          //.setContent("Izbrana to�ka:" + latlng.toString())
          //.openOn(mapa);

        prikazPoti(latlng);
    }

    mapa.on('click', obKlikuNaMapo);
    
    document.getElementById("izbrisiRezultate")
        .addEventListener("click", function () {
            // Odstrani vse oznake iz zemljevida
            for (var i = 1; i < markerji.length; i++) {
                mapa.removeLayer(markerji[i]);
            }
            // Odstrani vse oznake, razen FRI
            markerji.splice(1);
            // Onemogo�i gumb
            document.getElementById("izbrisiRezultate").disabled = true;
            // Ponovno omogo�i oba gumba za dodajanje
            document.getElementById("dodajFakultete").disabled = false;
            document.getElementById("dodajRestavracije").disabled = false;
            // Resetiraj �tevilo najdenih zadetkov
            document.getElementById("fakultete_rezultati").innerHTML = 0;
            document.getElementById("restavracije_rezultati").innerHTML = 0;
        });

    document.getElementById("idRadij")
        .addEventListener("click", function () {
            prikaziObmocje();
        });

    document.getElementById("dodajFakultete")
        .addEventListener("click", function () {
            dodajFakultete();
        });

    document.getElementById("dodajRestavracije")
        .addEventListener("click", function () {
            dodajRestavracije();
        });

    document.getElementById("radij")
        .addEventListener("keyup", function () {
            prikaziObmocje();
            posodobiOznakeNaZemljevidu();
        });
        */
});
/**
 * Na zemljevid dodaj oznake z bli�njimi fakultetami in
 * gumb onemogo�i.
 */
function dodajFakultete() {
    pridobiPodatke("fakultete", function (jsonRezultat) {
        izrisRezultatov(jsonRezultat);
        document.getElementById("dodajFakultete").disabled = true;
        document.getElementById("izbrisiRezultate").disabled = false;
    });
}


/**
 * Na zemljevid dodaj oznake z bli�njimi restavracijami in 
 * gumb onemogo�i.
 */
function dodajRestavracije() {
    pridobiPodatke("restavracije", function (jsonRezultat) {
        izrisRezultatov(jsonRezultat);
        document.getElementById("dodajRestavracije").disabled = true;
        document.getElementById("izbrisiRezultate").disabled = false;
    });
}


/**
 * Za podano vrsto interesne to�ke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 * 
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(vrstaInteresneTocke, callback) {
    if (typeof (vrstaInteresneTocke) != "string") return;

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN1/" +
        vrstaInteresneTocke + ".json", true);
    xobj.onreadystatechange = function () {
        // rezultat ob uspe�no prebrani datoteki
        if (xobj.readyState == 4 && xobj.status == "200") {
            var json = JSON.parse(xobj.responseText);

            // nastavimo ustrezna polja (�tevilo najdenih zadetkov)
            if (vrstaInteresneTocke == "fakultete") {
                document.getElementById("fakultete_rezultati").innerHTML = json['features'].length;
            } else if (vrstaInteresneTocke == "restavracije") {
                document.getElementById("restavracije_rezultati").innerHTML = json['features'].length;
            }

            // vrnemo rezultat
            callback(json);
        }
    };
    xobj.send(null);
}


/**
 * Dodaj izbrano oznako na zemljevid na dolo�enih GPS koordinatah,
 * z dodatnim opisom, ki se prika�e v obla�ku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rde�a, druge fakultete = modra in
 * restavracije = zelena)
 * 
 * @param lat zemljepisna �irina
 * @param lng zemljepisna dol�ina
 * @param opis sporo�ilo, ki se prika�e v obla�ku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, opis, tip) {
    var ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
            'marker-icon-2x-' +
            (tip == 'FRI' ? 'red' : (tip == 'restaurant' ? 'green' : 'blue')) +
            '.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
            'marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    // Ustvarimo marker z vhodnima podatkoma koordinat 
    // in barvo ikone, glede na tip
    var marker = L.marker([lat, lng], { icon: ikona });

    // Izpi�emo �eleno sporo�ilo v obla�ek
    marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

    // Dodamo to�ko na mapo in v seznam
    marker.addTo(mapa);
    markerji.push(marker);
}


/**
 * Na podlagi podanih interesnih to�k v GeoJSON obliki izri�i
 * posamezne to�ke na zemljevid
 * 
 * @param jsonRezultat interesne to�ke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
    var znacilnosti = jsonRezultat.features;

    for (var i = 0; i < znacilnosti.length; i++) {
        var jeObmocje =
            typeof (znacilnosti[i].geometry.coordinates[0]) == "object";
        var opis = znacilnosti[i].properties.name;

        // pridobimo koordinate
        var lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] :
            znacilnosti[i].geometry.coordinates[0];
        var lat = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] :
            znacilnosti[i].geometry.coordinates[1];
        if (prikaziOznako(lng, lat))
            dodajMarker(lat, lng, opis, znacilnosti[i].properties.amenity);
    }
}


/**
 * Glede na vrednost radija obmo�ja izbri�i oz. dodaj
 * oznake na zemljevid.
 */
function posodobiOznakeNaZemljevidu() {
    // FRI marker pustimo, ostale odstranimo
    for (var i = 1; i < markerji.length; i++) {
        mapa.removeLayer(markerji[i]);
    }
    markerji = [];
    dodajMarker(FRI_LAT, FRI_LNG, "FAKULTETA ZA RA�UNALNI�TVO IN INFORMATIKO", "FRI");
    if (document.querySelector("#dodajFakultete").disabled) {
        dodajFakultete();
    }
    if (document.querySelector("#dodajRestavracije").disabled) {
        dodajRestavracije();
    }
}


/**
 * Prikaz poti od/do izbrane lokacije do/od FRI
 * 
 * @param latLng izbrana to�ka na zemljevidu
 */
function prikazPoti(latLng) {
    // Izbri�emo obstoje�o pot, �e ta obstaja
    if (pot != null) mapa.removeControl(pot);

    if (document.getElementById("idDoFri").checked) {
        pot = L.Routing.control({
            waypoints: [
                latLng,
                L.latLng(FRI_LAT, FRI_LNG)
            ],
            lineOptions: {
                styles: [{
                    color: "#242c81",
                    weight: 12
                }]
            },
            language: "sl"
        });
    } else {
        pot = L.Routing.control({
            waypoints: [
                L.latLng(FRI_LAT, FRI_LNG),
                latLng
            ],
            lineOptions: {
                styles: [{
                    color: "#242c81",
                    weight: 12
                }]
            },
            language: "sl"
        });
    }
    pot.addTo(mapa);
}


/**
 * Preveri ali izbrano oznako na podanih GPS koordinatah izri�emo
 * na zemljevid glede uporabni�ko dolo�eno vrednost radij, ki
 * predstavlja razdaljo od FRI.
 * 
 * �e radij ni dolo�en, je enak 0 oz. je ve�ji od razdalje izbrane
 * oznake od FRI, potem oznako izri�emo, sicer ne.
 * 
 * @param lat zemljepisna �irina
 * @param lng zemljepisna dol�ina
 */
function prikaziOznako(lng, lat) {
    var radij = vrniRadij();
    if (radij == 0)
        return true;
    else if (distance(lat, lng, FRI_LAT, FRI_LNG, "K") >= radij)
        return false;
    else
        return true;
}


/**
 * Na zemljevidu nari�i rde� krog z transparentnim rde�im polnilom
 * s sredi��em na lokaciji FRI in radijem. Obmo�je se izri�e 
 * le, �e je na strani izbrana vrednost "Prikaz radija".
 */
function prikaziObmocje() {
    if (document.getElementById("idRadij").checked) {
        if (obmocje != null) mapa.removeLayer(obmocje);
        obmocje = L.circle([FRI_LAT, FRI_LNG], {
            color: 'red',
            fillColor: 'red',
            fillOpacity: 0.10,
            radius: vrniRadij() * 1000
        }).addTo(mapa);
    } else if (obmocje != null) {
        mapa.removeLayer(obmocje);
    }
}


/**
 * Vrni celo�tevilsko vrednost radija, ki ga uporabnik vnese v 
 * vnosno polje. �e uporabnik vnese neveljavne podatke, je
 * privzeta vrednost radija 0.
 */
function vrniRadij() {
    var vrednost = document.getElementById("radij");
    if (vrednost == null) {
        vrednost = 0;
    } else {
        vrednost = parseInt(vrednost.value, 10);
        vrednost = isNaN(vrednost) ? 0 : vrednost;
    }
    return vrednost;
    }
    